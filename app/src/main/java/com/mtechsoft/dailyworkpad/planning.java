package com.mtechsoft.dailyworkpad;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.ResponceModel.AddPlanResponceModel;
import com.mtechsoft.dailyworkpad.ViewModel.GetCourcesViewModel;
import com.mtechsoft.dailyworkpad.adapter.ProfessionalLearningAdapter;
import com.mtechsoft.dailyworkpad.dataModel.GetCourcesDataModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class planning extends Fragment implements ProfessionalLearningAdapter.CallBack {
    private RecyclerView category_recyclerview;
    private ProfessionalLearningAdapter adpter;
    private LinearLayoutManager manager;
    List<GetCourcesDataModel> models;
    public static final int REQUEST_IMAGE = 100;
    KProgressHUD dialog0;
    Context mcon;
    String imageEncoded, input = "";
    View v;
    //dialog box
    TextView ed_date;
    EditText ed_courseName, ed_PresenterName, ed_ProviderName;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    final Calendar myCalendar = Calendar.getInstance();
    ImageButton img_addProfessional;
    ImageView img_certificate;
    GetDateService service;
    String minuts = "", hours = "",
            imguribase64, TAG = "planning";
    public int itemPosition;
    GetCourcesViewModel courcesViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_planning, container, false);

        init(v);
        clicks(v);
        //load RecyclerView data
        setViewModels(v);
        return v;
    }

    //recyclerview data
    private void setViewModels(View v) {
        // Planing Observer
        Observer<List<GetCourcesDataModel>> activePlaningsListObserver = new Observer<List<GetCourcesDataModel>>() {
            @Override
            public void onChanged(List<GetCourcesDataModel> dataModels) {
                if (dataModels.size() == 0) {
                    Toast.makeText(mcon, "No Orders found!", Toast.LENGTH_SHORT).show();
                    //layoutRecentlySaved.setVisibility(View.GONE);
                } else {
                    //category_recyclerview.setHasFixedSize(true);
                    adpter = new ProfessionalLearningAdapter(getActivity(), dataModels, planning.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(mcon, LinearLayoutManager.VERTICAL, false);
                    category_recyclerview.setLayoutManager(layoutManager);
                    category_recyclerview.setAdapter(adpter);
                }

            }
        };


        // Working With Active Orders
        category_recyclerview = v.findViewById(R.id.category_recyclerview);
        courcesViewModel = new ViewModelProvider(this).get(GetCourcesViewModel.class);
        String userId = Utilities.getString(mcon, Utilities.USER_ID);
        //String userId="2";
        courcesViewModel.getAllCources(userId, getContext());
        courcesViewModel.getGetcourcesdata().observe(getViewLifecycleOwner(), activePlaningsListObserver);
    }

    private void clicks(View v) {
        img_addProfessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addnewItem();
            }
        });
    }

    private void init(View v) {
        models = new ArrayList<>();
        category_recyclerview = v.findViewById(R.id.category_recyclerview);
        img_addProfessional = v.findViewById(R.id.img_addProfessional);
        mcon = getActivity();
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
//        progressHUD = KProgressHUD.create(this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setWindowColor(getResources().getColor(R.color.colorBlack))
//                .setAnimationSpeed(3)
//                .setDimAmount(0.5f)
//                .setCancellable(true)
//                .setLabel("Please Wait")
//                .setCornerRadius(10)
//                .show();


    }


    private void addnewItem() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Dialog dialog;
        dialog = new Dialog(mcon);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_add_professional_planning);
        dialog.getWindow().setLayout((int) (width * 0.9), (int) (height * 0.90));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ed_date = dialog.findViewById(R.id.ed_date);
        ed_courseName = dialog.findViewById(R.id.ed_courseName);
        ed_PresenterName = dialog.findViewById(R.id.ed_PresenterName);
        ed_ProviderName = dialog.findViewById(R.id.ed_ProviderName);
        Button btn_continue = dialog.findViewById(R.id.btn_continue);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        img_certificate = dialog.findViewById(R.id.img_certificate);
        clicksOnDialog(imgclose, img_certificate, btn_continue, ed_date, dialog);


        dialog.setCancelable(false);
        dialog.show();
    }


    //total clicks on Dialog add
    private void clicksOnDialog(ImageView imgclose, ImageView img_certificate, Button btn_continue, TextView ed_date, Dialog dialog) {
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        img_certificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = ed_date.getText().toString().trim();
                String courseName = ed_courseName.getText().toString();
                String providerName = ed_ProviderName.getText().toString();
                String presenterName = ed_PresenterName.getText().toString();
                if (courseName.isEmpty()) {
                    ed_courseName.setError("*Date is Missing");
                    ed_courseName.requestFocus();
                } else if (date.isEmpty()) {
                    ed_date.setError("*enddate is Missing");
                    ed_date.requestFocus();
                } else if (providerName.isEmpty()) {
                    ed_ProviderName.setError("*sartdate is Missing");
                    ed_ProviderName.requestFocus();
                } else if (presenterName.isEmpty()) {
                    ed_PresenterName.setError("*sartdate is Missing");
                    ed_PresenterName.requestFocus();
                } else if (input.isEmpty()) {
                    Utilities.makeToast(mcon, "Please Select an Image");
                } else {
                    //progressLoading.show();
                    AddLearningCourse(date, courseName, providerName, presenterName, imguribase64, dialog);
                }
            }
        });


        //date picker
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        ed_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(mcon, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void AddLearningCourse(String date, String courseName, String providerName, String presenterName, String imguribase64, Dialog dialog) {

        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        String user_id = Utilities.getString(mcon, Utilities.USER_ID);

        Call<AddPlanResponceModel> call = service.addlearningcourse(user_id, imguribase64, date, courseName, providerName, presenterName);
        call.enqueue(new Callback<AddPlanResponceModel>() {
            @Override
            public void onResponse(Call<AddPlanResponceModel> call, Response<AddPlanResponceModel> response) {
                //progressLoading.hide();
                Log.d(TAG, "responce\t" + response);
                assert response.body() != null;
                int status = response.body().getStatus();
                Log.d(TAG, "status:\t" + status);
                String message = response.body().getMessage();
                if (status == 200) {
                    Utilities.makeToast(mcon, "Course Added Successfully" + status);
                    dialog0.dismiss();
                    // Working with response and saving data in Shared Preferences
                } else if (status == 400) {
                    //progressLoading.hide();
                    dialog0.dismiss();
                    Utilities.makeToast(mcon, "Failed to Add Course\n" + message);
                }

            }

            @Override
            public void onFailure(Call<AddPlanResponceModel> call, Throwable t) {
                dialog0.dismiss();
                Toast.makeText(mcon, t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "select a picture"), REQUEST_IMAGE);
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ed_date.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                img_certificate.setImageURI(uri);
                Log.d(TAG, "uri:\t" + uri);
                Log.d(TAG, "selectedImageUri:\t" + uri);
                //imageString=selectedImageUri.toString ();
                //Log.d (TAG,"String:\t"+imageString);

                Bitmap bitmapImage = null;
                // You can update this bitmap to your server
                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                input = imageEncoded;
                input = input.replace("\n", "");
                input = input.trim();
                input = "data:image/png;base64," + input;
                // loading profile image from local cache
                assert uri != null;
                //loadPhotos(uri.toString());
                imguribase64 = input;
                Log.d(TAG, "ImageURI:\t" + imguribase64);
            }
        }
    }

    @Override
    public void onGetPosition(int pos) {
        itemPosition = pos;
        Log.d(TAG, "Position:\t" + itemPosition);
    }
}