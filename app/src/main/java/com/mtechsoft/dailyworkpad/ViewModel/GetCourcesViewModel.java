package com.mtechsoft.dailyworkpad.ViewModel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.mtechsoft.dailyworkpad.ResponceModel.GetCourcesResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.GetCourcesDataModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetCourcesViewModel extends ViewModel {
    MutableLiveData<List<GetCourcesDataModel>> getcourcesdata = new MutableLiveData<>();

    public GetCourcesViewModel() {
    }

    public MutableLiveData<List<GetCourcesDataModel>> getGetcourcesdata() {
        return getcourcesdata;
    }

    public void setGetcourcesdata(MutableLiveData<List<GetCourcesDataModel>> getcourcesdata) {
        this.getcourcesdata = getcourcesdata;
    }

    public void getAllCources(String userId, final Context context) {

//
//        final IOSDialog progressLoading = new IOSDialog.Builder(context)
//                .setTitleColorRes(R.color.colorIphoneLoading)
//                .setSpinnerColorRes(R.color.colorIphoneLoading)
//                .setBackgroundColorRes(R.color.colorIphoneLoading)
//                .setMessageContent("Loading...")
//                .setMessageColorRes(R.color.colorIphoneLoading)
//                .build();
//        progressLoading.show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<GetCourcesResponceModel> call = service.get_learning_courses (userId);
        call.enqueue(new Callback<GetCourcesResponceModel> () {
            @Override
            public void onResponse(Call<GetCourcesResponceModel> call, Response<GetCourcesResponceModel> response) {
                //             progressLoading.dismiss();
//                assert response.body() != null;
                if (response.body() != null) {
                    int status = response.body().getStatus();
                    Log.d ("GetPlan","Status"+status);
                    if (status == 200) {
                        Utilities.makeToast (context,"Error:\t"+status);
                        // progressLoading.dismiss();
                        List<GetCourcesDataModel> cources = response.body().getCourcesDataModels ();
                        GetCourcesResponceModel responseModel = new GetCourcesResponceModel();
                        responseModel.setCourcesDataModels (cources);
                        getcourcesdata.setValue(cources);

                    } else if (status == 400) {
                        Utilities.makeToast (context,"Error:\t"+status);
//                        progressLoading.dismiss();

                    }

                }
            }

            @Override
            public void onFailure(Call<GetCourcesResponceModel> call, Throwable t) {
                //progressLoading.hide();
                t.printStackTrace();
            }
        });


    }
}
