package com.mtechsoft.dailyworkpad.ViewModel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.mtechsoft.dailyworkpad.ResponceModel.GetPlaningResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.GetPlaningDataModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetPlaningViewModel  extends ViewModel {
    MutableLiveData<List<GetPlaningDataModel>> getPlaning = new MutableLiveData<>();

    public GetPlaningViewModel() {
    }

    public MutableLiveData<List<GetPlaningDataModel>> getGetPlaning() {
        return getPlaning;
    }

    public void setGetPlaning(MutableLiveData<List<GetPlaningDataModel>> getPlaning) {
        this.getPlaning = getPlaning;
    }

    public void getAllPlanings(String userId, final Context context) {

//
//        final IOSDialog progressLoading = new IOSDialog.Builder(context)
//                .setTitleColorRes(R.color.colorIphoneLoading)
//                .setSpinnerColorRes(R.color.colorIphoneLoading)
//                .setBackgroundColorRes(R.color.colorIphoneLoading)
//                .setMessageContent("Loading...")
//                .setMessageColorRes(R.color.colorIphoneLoading)
//                .build();
//        progressLoading.show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<GetPlaningResponceModel> call = service.getPlannings (userId);
        call.enqueue(new Callback<GetPlaningResponceModel> () {
            @Override
            public void onResponse(Call<GetPlaningResponceModel> call, Response<GetPlaningResponceModel> response) {
   //             progressLoading.dismiss();
//                assert response.body() != null;
                if (response.body() != null) {
                    int status = response.body().getStatus();
                    Log.d ("GetPlan","Status"+status);
                    if (status == 200) {
                        Utilities.makeToast (context,"Error:\t"+status);
                       // progressLoading.dismiss();
                        List<GetPlaningDataModel> activeOrders = response.body().getPlaningdatamodel ();
                        GetPlaningResponceModel responseModel = new GetPlaningResponceModel();
                        responseModel.setPlaningdatamodel (activeOrders);
                        getPlaning.setValue(activeOrders);

                    } else if (status == 400) {
                        Utilities.makeToast (context,"Error:\t"+status);
//                        progressLoading.dismiss();

                    }

                }
            }

            @Override
            public void onFailure(Call<GetPlaningResponceModel> call, Throwable t) {
                //progressLoading.hide();
                t.printStackTrace();
            }
        });


    }
}
