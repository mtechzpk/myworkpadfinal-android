package com.mtechsoft.dailyworkpad.networks;


import com.mtechsoft.dailyworkpad.ResponceModel.AddPlanResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.AuthenticationresponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.CommonResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.ForgetPasswordResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.GetCourcesResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.GetMeetingsResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.GetPlaningResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.GetTasksResponceModel;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetDateService {

    //Register User
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("signup")
    Call<AuthenticationresponceModel> signupUser(@Field("name") String name,
                                                 @Field("email") String email,
                                                 @Field("password") String password,
                                                 @Field("password_confirmation") String password_confirmation
    );

    //Login User

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("login")
    Call<AuthenticationresponceModel> loginUser ( @Field("email") String email,
                                        @Field("password") String password
    );


    //forgot_password

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("forgot_password")
    Call<ForgetPasswordResponceModel> forgot_passwordUser (@Field("email") String email);





    //add_planning
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("add_planning")
    Call<AddPlanResponceModel> addPlanning(@Field("user_id") String user_id,
                                           @Field("planning_date") String planning_date,
                                           @Field("planning_hyperlink") String planning_hyperlink,
                                           @Field("planning_description") String planning_description,
                                           @Field("start_time") String start_time,
                                           @Field("end_time") String end_time
    );




    //get_plannings
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_plannings")
    Call<GetPlaningResponceModel> getPlannings(@Field("user_id") String user_id);

    //deleteplanning
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("delete_planning")
    Call<CommonResponceModel> deleteplanning(@Field("planning_id") String planning_id);




    //add_learning_course
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("add_learning_course")
    Call<AddPlanResponceModel> addlearningcourse(@Field("user_id") String user_id,
                                                 @Field("certificate_image") String certificate_image,
                                                 @Field("course_date") String course_date,
                                                 @Field("course_name") String course_name,
                                                 @Field("provider_name") String provider_name,
                                                 @Field("presenter_name") String presenter_name
    );
    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("filter_task_group")
    Call<GetTasksResponceModel> filter_task_group(@Field("task_type") String task_type,
                                                  @Field("task_group_id") String task_group_id

    );

    //get_learning_courses
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_learning_courses")
    Call<GetCourcesResponceModel> get_learning_courses(@Field("user_id") String user_id);

    //deleteplanning
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("delete_learning_course")
    Call<CommonResponceModel> deletelearningcourse(@Field("course_id") String course_id);





    //get_meetings
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_meetings")
    Call<GetMeetingsResponceModel> get_meetings(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("get_meetings")
    Call<ResponseBody> get_meeting(

            @Field("user_id") String user_id,
            @HeaderMap Map<String, String> headers
    );


    //get_tasks
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_tasks")
    Call<GetTasksResponceModel> get_tasks(@Field("user_id") String user_id);






    //delete_task_group
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("delete_task_group")
    Call<CommonResponceModel> delete_task_group(@Field("task_group_id") String task_group_id);


    //add_tasks
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("add_tasks")
    Call<AddPlanResponceModel> addTasks(@Field("user_id") String user_id,
                                                 @Field("task_group_name") String task_group_name,
                                                 @Field("task_group_date") String task_group_date,
                                                 @Field("tasks") String tasks
    );


    //add_meeting
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("add_meeting")
    Call<AddPlanResponceModel> addMeeting(@Field("user_id") String user_id,
                                        @Field("meeting_note_title") String meeting_note_title,
                                        @Field("meeting_note_date") String meeting_note_date,
                                        @Field("meetings") String meetings
    );

    //delete_meeting
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("delete_meeting")
    Call<CommonResponceModel> deleteMeeting(@Field("meeting_id") String meeting_id);

    //delete_single_task
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("delete_single_task")
    Call<CommonResponceModel> deletesingleTask(@Field("task_id") String task_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("complete_task")
    Call<CommonResponceModel> complete_task(@Field("task_id") String task_id);
}
