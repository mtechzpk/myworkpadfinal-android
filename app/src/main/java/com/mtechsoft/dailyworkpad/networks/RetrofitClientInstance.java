package com.mtechsoft.dailyworkpad.networks;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

        private static Retrofit retrofit;
        public static final String BASE_URL = "http://nin9teens.com/task_management/public/api/";
        public static final String BASE_URL_IMG = "http://nin9teens.com/task_management/public/";

        public static Retrofit getRetrofitInstance() {
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
}
