package com.mtechsoft.dailyworkpad.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.ForgetPasswordResponceModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {
    Button btn_next;
    EditText ed_Email;
    GetDateService service;
    AVLoadingIndicatorView progressLoading;
    String TAG="ForgetPasswordActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_forget_password);
        init();
        clicks();
    }

    private void clicks() {
        btn_next.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                String userEmail = ed_Email.getText().toString();
                boolean isValidEmail = Utilities.isValidEmail(userEmail);
                if (userEmail.isEmpty()) {
                    ed_Email.setError("*Email is Missing");
                    ed_Email.requestFocus();
                } else if (!isValidEmail) {
                    ed_Email.setError("*Email is not valid");
                    ed_Email.requestFocus();
                }
                else {
                    progressLoading.show();
                    forgetPasword(userEmail);
                }
            }
        });
    }

    private void forgetPasword(String userEmail) {
        Call<ForgetPasswordResponceModel> call = service.forgot_passwordUser (userEmail);
        call.enqueue(new Callback<ForgetPasswordResponceModel> () {
            @Override
            public void onResponse(Call<ForgetPasswordResponceModel> call, Response<ForgetPasswordResponceModel> response) {
                progressLoading.hide();
                Log.d (TAG,"responce\t"+response);
                assert response.body() != null;
                int status = response.body().getStatus();
                String message  = response.body().getMessage ();
                if (status == 200) {
                    Utilities.makeToast (getApplicationContext (),"Kindly Check Your Email\n"+message);

                    clearViews();
                    startActivity(new Intent (ForgetPasswordActivity.this, OTPActivity.class));
                    finish();
                } else if (status == 400) {
                    progressLoading.hide();
                    Utilities.makeToast (getApplicationContext (),"Failed\n"+message);
                }

            }

            @Override
            public void onFailure(Call<ForgetPasswordResponceModel> call, Throwable t) {
                progressLoading.hide();
                Log.d (TAG,"responce\t"+t.getMessage ());
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }




    private void clearViews() {
        ed_Email.setText("");
    }

    private void init() {
        progressLoading = findViewById(R.id.loading);
        btn_next=findViewById (R.id.btn_next);
        ed_Email=findViewById (R.id.ed_Email);
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
    }

    public void LogIn(View view) {
        startActivity (new Intent (getApplicationContext (),LogInActivity.class));
    }
}