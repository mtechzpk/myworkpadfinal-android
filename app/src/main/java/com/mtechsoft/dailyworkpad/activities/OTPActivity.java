package com.mtechsoft.dailyworkpad.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.chaos.view.PinView;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.ForgetPasswordResponceModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity {
    Button btn_next;
    PinView pinView;
    String otpCode;
    GetDateService service;
    AVLoadingIndicatorView progressLoading;
    String TAG="OTPActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_o_t_p);



        init();
        clicks();
    }

    private void clicks() {
        btn_next.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void forgetPasword(String userEmail) {
        Call<ForgetPasswordResponceModel> call = service.forgot_passwordUser (userEmail);
        call.enqueue(new Callback<ForgetPasswordResponceModel> () {
            @Override
            public void onResponse(Call<ForgetPasswordResponceModel> call, Response<ForgetPasswordResponceModel> response) {
                progressLoading.hide();
                Log.d (TAG,"responce\t"+response);
                assert response.body() != null;
                int status = response.body().getStatus();
                String message  = response.body().getMessage ();
                if (status == 200) {
                    Utilities.makeToast (getApplicationContext (),"Kindly Check Your Email\n"+message);

                    clearViews();
                    startActivity(new Intent (OTPActivity.this, OTPActivity.class));
                    finish();
                } else if (status == 400) {
                    progressLoading.hide();
                    Utilities.makeToast (getApplicationContext (),"Failed\n"+message);
                }

            }

            @Override
            public void onFailure(Call<ForgetPasswordResponceModel> call, Throwable t) {
                progressLoading.hide();
                Log.d (TAG,"responce\t"+t.getMessage ());
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }




    private void clearViews() {
        pinView.setText("");
    }

    private void init() {
        progressLoading = findViewById(R.id.loading);
        btn_next=findViewById (R.id.btn_next);
        pinView=findViewById (R.id.pinView);
        String userEmail = getIntent().getExtras().getString("email");
        Log.d (TAG,"Email:\t"+userEmail);
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
    }

    public void LogIn(View view) {
        startActivity (new Intent (getApplicationContext (),LogInActivity.class));
    }
}