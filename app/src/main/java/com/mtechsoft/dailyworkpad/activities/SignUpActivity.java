package com.mtechsoft.dailyworkpad.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mtechsoft.dailyworkpad.Home_Screen;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.AuthenticationresponceModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    Button btn_signup;
    EditText ed_Name,ed_Email;
    ShowHidePasswordEditText ed_Password,ed_ConfPassword;
    GetDateService service;
    AVLoadingIndicatorView progressLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_sign_up);


        init();
        clicks();
    }

    private void clicks() {
        btn_signup.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                String userEmail = ed_Email.getText().toString();
                String userName = ed_Name.getText().toString();
                String userPassword = ed_Password.getText().toString();
                String confirmPassword = ed_ConfPassword.getText().toString();
                boolean isValidEmail = Utilities.isValidEmail(userEmail);
                if (userName.isEmpty()) {
                    ed_Name.setError("*Email is Missing");
                    ed_Name.requestFocus();
                } else if (userEmail.isEmpty()) {
                    ed_Email.setError("*Email is Missing");
                    ed_Email.requestFocus();
                } else if (!isValidEmail) {
                    ed_Email.setError("*Email is not valid");
                    ed_Email.requestFocus();
                } else if (userPassword.isEmpty()) {
                    ed_Password.setError("*Password is Missing");
                    ed_Password.requestFocus();
                } else if (confirmPassword.isEmpty()) {
                    ed_ConfPassword.setError("*Password is Missing");
                    ed_ConfPassword.requestFocus();
                } else if (userPassword.length() < 6) {
                    ed_ConfPassword.setError("*Must be at least 6 characters");
                    ed_ConfPassword.requestFocus();
                } else if (!userPassword.equals(confirmPassword)) {
                    ed_ConfPassword.setError("*Passwords are miss matched");
                    ed_ConfPassword.requestFocus();
                } else {
                    progressLoading.show();
                    userSignUp(userName, userEmail, userPassword, confirmPassword);
                }
            }
        });
    }

    private void userSignUp(String userName, String userEmail, String userPassword, String confirmPassword) {
        Call<AuthenticationresponceModel> call = service.signupUser (userName, userEmail, userPassword, confirmPassword);
        call.enqueue(new Callback<AuthenticationresponceModel> () {
            @Override
            public void onResponse(Call<AuthenticationresponceModel> call, Response<AuthenticationresponceModel> response) {
                progressLoading.hide();
                assert response.body() != null;
                int status = response.body().getStatus();
                String message  = response.body().getMessage ();
                if (status == 200) {
                    Utilities.makeToast (getApplicationContext (),"User Registered Successfully"+status);
                    // Working with response and saving data in Shared Preferences
                    int userId = response.body().getUserData().getId ();
                    String userName = response.body().getUserData().getName ();
                    String userEmail = response.body().getUserData().getEmail ();
                    Utilities.saveString (getApplicationContext (),Utilities.USER_EMAIL,userEmail);
                    Utilities.saveString (getApplicationContext (),Utilities.USER_NAME,userName);
                    Utilities.saveString (getApplicationContext (),Utilities.USER_ID,String.valueOf (userId));
                    clearViews();
                    Utilities.saveString (getApplicationContext (),Utilities.Login_Status,"login");
                    startActivity(new Intent (SignUpActivity.this, Home_Screen.class));
                    finish();
                } else if (status == 400) {
                    progressLoading.hide();
                    Utilities.makeToast (getApplicationContext (),"Failed to Register User\n"+message);
                }

            }

            @Override
            public void onFailure(Call<AuthenticationresponceModel> call, Throwable t) {
                //progressLoading.hide();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void clearViews() {
        ed_Email.setText("");
        ed_Name.setText("");
        ed_Password.setText("");
        ed_ConfPassword.setText("");
    }

    private void init() {
        progressLoading = findViewById(R.id.loading);
        btn_signup=findViewById (R.id.btn_signup);
        ed_Name=findViewById (R.id.ed_Name);
        ed_Email=findViewById (R.id.ed_Email);
        ed_Password=findViewById (R.id.ed_Password);
        ed_ConfPassword=findViewById (R.id.ed_ConfPassword);
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
    }

    public void SignIn(View view) {
        startActivity (new Intent (getApplicationContext (),LogInActivity.class));
    }
}