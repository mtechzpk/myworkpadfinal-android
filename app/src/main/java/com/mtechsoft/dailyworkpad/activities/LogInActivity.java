package com.mtechsoft.dailyworkpad.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.Home_Screen;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.AuthenticationresponceModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {
    Button btn_login;
    EditText ed_Email;
    KProgressHUD progressLoading;
    ShowHidePasswordEditText ed_Password;
    GetDateService service;
    String TAG="LogInActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_log_in);



        init();
        clicks();
    }

    private void clicks() {
        btn_login.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                String userEmail = ed_Email.getText().toString();
                String userPassword = ed_Password.getText().toString();
                boolean isValidEmail = Utilities.isValidEmail(userEmail);
                if (userEmail.isEmpty()) {
                    ed_Email.setError("*Email is Missing");
                    ed_Email.requestFocus();
                } else if (!isValidEmail) {
                    ed_Email.setError("*Email is not valid");
                    ed_Email.requestFocus();
                } else if (userPassword.isEmpty()) {
                    ed_Password.setError("*Password is Missing");
                    ed_Password.requestFocus();
                } else {
                    userSignIn(userEmail, userPassword);
                }
            }
        });
    }

    private void userSignIn(String userEmail, String userPassword) {
        progressLoading = KProgressHUD.create(LogInActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        Call<AuthenticationresponceModel> call = service.loginUser (userEmail, userPassword);
        call.enqueue(new Callback<AuthenticationresponceModel> () {
            @Override
            public void onResponse(Call<AuthenticationresponceModel> call, Response<AuthenticationresponceModel> response) {
                progressLoading.dismiss();
                Log.d (TAG,"responce\t"+response);
                assert response.body() != null;
                int status = response.body().getStatus();
                String message  = response.body().getMessage ();
                if (status == 200) {
                    progressLoading.dismiss();
                    Utilities.makeToast (getApplicationContext (),"User Signed In Successfully"+status);
                    // Working with response and saving data in Shared Preferences
                    int userId = response.body().getUserData().getId ();
                    String userName = response.body().getUserData().getName ();
                    String userEmail = response.body().getUserData().getEmail ();
                    Utilities.saveString (getApplicationContext (),Utilities.USER_EMAIL,userEmail);
                    Utilities.saveString (getApplicationContext (),Utilities.USER_NAME,userName);
                    Utilities.saveString (getApplicationContext (),Utilities.USER_ID,String.valueOf (userId));
                    Utilities.saveString (getApplicationContext (),Utilities.Login_Status,"logged");
                    clearViews();
                    startActivity(new Intent (LogInActivity.this, Home_Screen.class));
                    finish();
                } else if (status == 400) {
                    progressLoading.dismiss();
                    Utilities.makeToast (getApplicationContext (),"Failed to Register User\n"+message);
                }

            }

            @Override
            public void onFailure(Call<AuthenticationresponceModel> call, Throwable t) {
                progressLoading.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void clearViews() {
        ed_Email.setText("");
        ed_Password.setText("");
    }

    private void init() {
        btn_login=findViewById (R.id.btn_login);
        ed_Email=findViewById (R.id.ed_Email);
        ed_Password=findViewById (R.id.ed_Password);
        ed_Email.setText ("kamranabrar90@gmail.com");
        ed_Password.setText ("12345678");
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
    }

    public void SignIn(View view) {
        startActivity (new Intent (getApplicationContext (),LogInActivity.class));
    }

    public void forgetPassword(View view) {
        startActivity (new Intent (getApplicationContext (),ForgetPasswordActivity.class));
    }

    public void SignUp(View view) {
        startActivity (new Intent (getApplicationContext (),SignUpActivity.class));
    }

}