package com.mtechsoft.dailyworkpad.ResponceModel;

import com.google.gson.annotations.SerializedName;

public class CommonResponceModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;

    public CommonResponceModel(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
