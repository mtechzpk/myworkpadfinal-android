package com.mtechsoft.dailyworkpad.ResponceModel;

import com.google.gson.annotations.SerializedName;
import com.mtechsoft.dailyworkpad.dataModel.GetTasksDataModel;

import java.util.List;

public class GetTasksResponceModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<GetTasksDataModel> tasksDataModels;


    public GetTasksResponceModel() {
    }

    public GetTasksResponceModel(int status, String message, List<GetTasksDataModel> tasksDataModels) {
        this.status = status;
        this.message = message;
        this.tasksDataModels = tasksDataModels;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetTasksDataModel> getTasksDataModels() {
        return tasksDataModels;
    }

    public void setTasksDataModels(List<GetTasksDataModel> tasksDataModels) {
        this.tasksDataModels = tasksDataModels;
    }
}
