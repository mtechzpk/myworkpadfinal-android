package com.mtechsoft.dailyworkpad.ResponceModel;

import com.google.gson.annotations.SerializedName;

public class AddPlanResponceModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private String userData;

    public AddPlanResponceModel() {
    }

    public AddPlanResponceModel(int status, String message, String userData) {
        this.status = status;
        this.message = message;
        this.userData = userData;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserData() {
        return userData;
    }

    public void setUserData(String userData) {
        this.userData = userData;
    }
}
