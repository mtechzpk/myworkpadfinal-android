package com.mtechsoft.dailyworkpad.ResponceModel;

import com.google.gson.annotations.SerializedName;
import com.mtechsoft.dailyworkpad.dataModel.UserDataModel;

public class AuthenticationresponceModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private UserDataModel userData;

    public AuthenticationresponceModel() {
    }

    public AuthenticationresponceModel(int status, String message, UserDataModel userData) {
        this.status = status;
        this.message = message;
        this.userData = userData;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDataModel getUserData() {
        return userData;
    }

    public void setUserData(UserDataModel userData) {
        this.userData = userData;
    }
}
