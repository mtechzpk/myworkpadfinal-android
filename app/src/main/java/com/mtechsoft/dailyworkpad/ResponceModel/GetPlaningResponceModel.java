package com.mtechsoft.dailyworkpad.ResponceModel;

import com.google.gson.annotations.SerializedName;
import com.mtechsoft.dailyworkpad.dataModel.GetPlaningDataModel;

import java.util.List;

public class GetPlaningResponceModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<GetPlaningDataModel> planingdatamodel;

    public GetPlaningResponceModel() {
    }

    public GetPlaningResponceModel(int status, String message, List<GetPlaningDataModel> planingdatamodel) {
        this.status = status;
        this.message = message;
        this.planingdatamodel = planingdatamodel;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetPlaningDataModel> getPlaningdatamodel() {
        return planingdatamodel;
    }

    public void setPlaningdatamodel(List<GetPlaningDataModel> planingdatamodel) {
        this.planingdatamodel = planingdatamodel;
    }
}
