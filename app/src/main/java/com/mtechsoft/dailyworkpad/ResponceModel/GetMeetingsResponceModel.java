package com.mtechsoft.dailyworkpad.ResponceModel;

import com.google.gson.annotations.SerializedName;
import com.mtechsoft.dailyworkpad.dataModel.GetMeetingsDataModel;

import java.util.List;

public class GetMeetingsResponceModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<GetMeetingsDataModel> getMeetingsDataModel;

    public GetMeetingsResponceModel() {
    }

    public GetMeetingsResponceModel(int status, String message, List<GetMeetingsDataModel> getMeetingsDataModel) {
        this.status = status;
        this.message = message;
        this.getMeetingsDataModel = getMeetingsDataModel;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetMeetingsDataModel> getGetMeetingsDataModel() {
        return getMeetingsDataModel;
    }

    public void setGetMeetingsDataModel(List<GetMeetingsDataModel> getMeetingsDataModel) {
        this.getMeetingsDataModel = getMeetingsDataModel;
    }
}
