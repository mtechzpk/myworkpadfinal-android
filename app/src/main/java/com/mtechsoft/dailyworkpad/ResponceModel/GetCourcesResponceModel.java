package com.mtechsoft.dailyworkpad.ResponceModel;

import com.google.gson.annotations.SerializedName;
import com.mtechsoft.dailyworkpad.dataModel.GetCourcesDataModel;

import java.util.List;

public class GetCourcesResponceModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<GetCourcesDataModel> courcesDataModels;

    public GetCourcesResponceModel() {
    }

    public GetCourcesResponceModel(int status, String message, List<GetCourcesDataModel> courcesDataModels) {
        this.status = status;
        this.message = message;
        this.courcesDataModels = courcesDataModels;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetCourcesDataModel> getCourcesDataModels() {
        return courcesDataModels;
    }

    public void setCourcesDataModels(List<GetCourcesDataModel> courcesDataModels) {
        this.courcesDataModels = courcesDataModels;
    }
}
