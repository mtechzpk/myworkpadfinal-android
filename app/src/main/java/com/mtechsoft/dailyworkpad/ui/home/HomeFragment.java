package com.mtechsoft.dailyworkpad.ui.home;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.AddPlanResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.CommonResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.GetTasksResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.GetTasksDataModel;
import com.mtechsoft.dailyworkpad.dataModel.TaskListDataModel;
import com.mtechsoft.dailyworkpad.nestedRecyclerviewAdapter.ParentItemAdapterForTasks;
import com.mtechsoft.dailyworkpad.nested_recyclerviewModel.ParentItem;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.ui.gallery.GalleryViewModel;
import com.mtechsoft.dailyworkpad.utilities.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ParentItemAdapterForTasks.CallBack {
    public static final int REQUEST_IMAGE = 100;
    KProgressHUD progressHUD;
    int datePicker = 0;
    Context mcon;
    List<String> categories;
    View v;
    KProgressHUD progressDialog;
    KProgressHUD dialog0;
    String user_id = "";
    List<GetTasksDataModel> datalist;
    String[] spType = {"completed", "incompleted"};
    Spinner spinnerGroup;
    //dialog box
    TextView ed_date;
    SwipeRefreshLayout mSwipeRefreshLayout;
    EditText ed_tasktitle, ed_task1, ed_task2;
    RelativeLayout rl_addmore, rl_removetask;
    ScrollView scrollableView;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    final Calendar myCalendar = Calendar.getInstance();
    ImageButton img_addProfessional;
    String minuts = "", hours = "",
            imguribase64, TAG = "HomeFragment";
    private GalleryViewModel galleryViewModel;
    LinearLayout ll_taskdelete, ll_edittext;
    private HomeViewModel homeViewModel;
    List<ParentItem> itemList;
    LinearLayoutManager layoutManager;
    ParentItemAdapterForTasks adapter;
    RecyclerView ParentRecyclerViewItem;
    GetDateService service;
    List<GetTasksDataModel> parentmodels;
    List<TaskListDataModel> childModels;
    String tasks = null;
    static int counter = 0;
    int itemPosition;
    List<EditText> myArray;
    StringBuilder builder;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_home, container, false);
        user_id = Utilities.getString(getActivity(), Utilities.USER_ID);
        init(v);
        getTaskApi(user_id);
        clicks(v);
        return v;
    }


    private void getTaskApi(String user_id) {

        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<GetTasksResponceModel> call = service.get_tasks(user_id);
        call.enqueue(new Callback<GetTasksResponceModel>() {
            @Override
            public void onResponse(Call<GetTasksResponceModel> call, Response<GetTasksResponceModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                datalist = response.body().getTasksDataModels();
                if (status == 200) {
                    dialog0.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < datalist.size(); i++) {
                        String groupnAME = datalist.get(i).getTask_group_name();
//                        categories.add(groupnAME);
                    }
                    setData(ParentRecyclerViewItem, datalist);

//
//                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, categories);
//                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                    spinnerGroup.setAdapter(dataAdapter);
////                        categories.add(0, "Select Category");
////                        spCategories.setSelection(0);
//                    spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                            String get_area_type = parent.getItemAtPosition(position).toString();
////                            cat_id = vendorCategoryModels.get(position).getId();
////                            getSubCategoriesApi(String.valueOf(cat_id));
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> parent) {
//
//                        }
//                    });

                } else {
                    dialog0.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetTasksResponceModel> call, Throwable t) {

                dialog0.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void setData(RecyclerView rvAvailableDate, List datalist) {

        adapter = new ParentItemAdapterForTasks(datalist, getActivity(), HomeFragment.this);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        rvAvailableDate.setHasFixedSize(true);
        rvAvailableDate.setLayoutManager(manager);
        rvAvailableDate.setAdapter(adapter);
    }

    private void clicks(View v) {
        img_addProfessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addnewItem();
//                Intent intent=new Intent (mcon, AddProfessionalPlanningActivity.class);
//                startActivity (intent);

            }
        });

    }

    private void init(View v) {
        myArray = new ArrayList();
        img_addProfessional = v.findViewById(R.id.img_addProfessional);
        mSwipeRefreshLayout = v.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mcon = getActivity();
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        ParentRecyclerViewItem = v.findViewById(R.id.parent_recyclerview);
        parentmodels = new ArrayList<>();
        childModels = new ArrayList<>();
// Initialise the Linear layout manager
        layoutManager = new LinearLayoutManager(mcon);
//        ParentRecyclerViewItem.addOnItemTouchListener(new RecyclerTouchListener(mcon, ParentRecyclerViewItem, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//                optionsMenuDialog(mcon);
//            }
//        }));
    }


    private void addnewItem() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Dialog dialog;
        dialog = new Dialog(mcon);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_task);
        dialog.getWindow().setLayout((int) (width * 0.9), WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ed_date = dialog.findViewById(R.id.ed_date);
        RelativeLayout rl_addmore, rl_removetask;
        ed_tasktitle = dialog.findViewById(R.id.ed_tasktitle);
        ed_task2 = dialog.findViewById(R.id.ed_task2);
        ed_task1 = dialog.findViewById(R.id.ed_task1);
        rl_addmore = dialog.findViewById(R.id.rl_addmore);
        rl_removetask = dialog.findViewById(R.id.rl_removetask);
        Button btn_addTask = dialog.findViewById(R.id.btn_addTask);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        ll_taskdelete = dialog.findViewById(R.id.ll_taskdelete);
        ll_edittext = dialog.findViewById(R.id.ll_edittext);
        clicksOnDialog(ed_task1, ed_tasktitle, imgclose, btn_addTask, ed_date, dialog, rl_addmore, rl_removetask, ll_edittext);


        dialog.setCancelable(false);
        dialog.show();
    }

    private void clicksOnDialog(EditText ed_task1, EditText ed_tasktitle, ImageView imgclose, Button btn_addTask, TextView ed_date, Dialog dialog, RelativeLayout rl_addmore, RelativeLayout rl_removetask, LinearLayout ll_edittext) {
        rl_addmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "editText");
                addNewEditText();
            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        rl_removetask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_taskdelete.setVisibility(View.GONE);
            }
        });

        //date picker
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        ed_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(mcon, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = ed_date.getText().toString().trim();
                String taskTitle = ed_tasktitle.getText().toString();
                String task1 = ed_task1.getText().toString();
                Log.d(TAG, "Length:\t" + myArray.size());
                builder = new StringBuilder(task1);
                int i = 0;
                while (i < myArray.size()) {
                    Log.d(TAG, "Text for :\t" + myArray.get(i).getText().toString());
                    String finalString = myArray.get(i).getText().toString();
                    if (!finalString.isEmpty()) builder.append("," + finalString);
                    i++;
                }
                Log.d(TAG, "builder :\t" + builder);
                if (date.isEmpty()) {
                    ed_date.setError("*Missing");
                    ed_date.requestFocus();
                } else if (taskTitle.isEmpty()) {
                    ed_tasktitle.setError("*Missing");
                    ed_tasktitle.requestFocus();
                } else if (builder == null || builder.length() == 0) {
                    ed_task1.setError("*Missing");
                    ed_task1.requestFocus();
                } else {
                    AddTaskApiCall(date, taskTitle, builder, dialog);
                }
            }
        });
    }

    private void AddTaskApiCall(String date, String taskTitle, StringBuilder tasksList, Dialog dialog) {

        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        user_id = Utilities.getString(mcon, Utilities.USER_ID);
        Call<AddPlanResponceModel> call = service.addTasks(user_id, taskTitle, date, tasksList.toString());
        call.enqueue(new Callback<AddPlanResponceModel>() {
            @Override
            public void onResponse(Call<AddPlanResponceModel> call, Response<AddPlanResponceModel> response) {
                dialog0.dismiss();
                Log.d(TAG, "responce\t" + response);
                assert response.body() != null;
                int status = response.body().getStatus();
                Log.d(TAG, "status:\t" + status);
                String message = response.body().getMessage();
                if (status == 200) {
                    Utilities.makeToast(mcon, "Course Added Successfully" + status);
                    dialog0.dismiss();
                    dialog.dismiss();
                    getTaskApi(user_id);
                    // Working with response and saving data in Shared Preferences
                } else if (status == 400) {
                    //progressLoading.hide();
                    dialog0.dismiss();
                    Utilities.makeToast(mcon, "Failed to Add Course\n" + message);
                }

            }

            @Override
            public void onFailure(Call<AddPlanResponceModel> call, Throwable t) {
                dialog0.dismiss();
                Toast.makeText(mcon, t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addNewEditText() {
        EditText editText = new EditText(mcon);
        String abc = editText.toString() + counter;
        editText.setId(counter);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 8, 20, 8);
        editText.setLayoutParams(params);
        editText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        editText.setBackground(getResources().getDrawable(R.drawable.bg_edittext_shape));
        editText.setHint("Add Task");
        editText.setTextSize(20);
        //editText.setTextCursorDrawable (getResources ().getDrawable (R.drawable.edit_cursor_color));
        editText.setPadding(10, 12, 10, 12);
        ll_edittext.addView(editText);
        myArray.add(editText);
    }


    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ed_date.setText(sdf.format(myCalendar.getTime()));
    }

    private List<GetTasksDataModel> ParentItemList(List<GetTasksDataModel> parentmodels) {
        int i = 0;
        Log.d(TAG, "Size:\t" + parentmodels.size());
        for (i = 0; i <= parentmodels.size() - 1; i++) {
            int id = parentmodels.get(i).getId();
            String task_group_name = parentmodels.get(i).getTask_group_name();
            String created_at = parentmodels.get(i).getCreated_at();
            Log.d(TAG, "meeting_note_title:\t" + task_group_name);
            childModels = parentmodels.get(i).getTaskListDataModels();
        }
        return this.parentmodels;
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                onRefreshfrag();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        }, 1000);

//        Toast.makeText(context, "jdfjdf", Toast.LENGTH_SHORT).show();
    }

    public void onRefreshfrag() {
        getFragmentManager().beginTransaction().detach(this).attach(this).commit();

    }

    @Override
    public void onGetPosition(int pos) {
        int taskid = datalist.get(pos).getId();
        deleteDialog(taskid, pos);
    }

    private void deleteDialog(int id, int postion) {
        final PrettyDialog pDialog = new PrettyDialog(getActivity());
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete this Group?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_gray,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deletegrouppApi(id, postion);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_gray,
                        R.color.colorWhite,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void deletegrouppApi(int idd, int position) {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CommonResponceModel> call = service.delete_task_group(String.valueOf(idd));

        call.enqueue(new Callback<CommonResponceModel>() {
            @Override
            public void onResponse(Call<CommonResponceModel> call, Response<CommonResponceModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    getTaskApi(user_id);
                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponceModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void filterDialog() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.task_filter);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.50)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnFilter = dialog.findViewById(R.id.btnFilter);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        Spinner spinnerType = dialog.findViewById(R.id.spinnerType);
        spinnerGroup = dialog.findViewById(R.id.spinnerGroup);

        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, spType);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnerType.setAdapter(aa);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String getType = spType[position];
                Toast.makeText(getActivity(), spType[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                serchFilterApi()
            }
        });
        dialog.show();
    }

    private void serchFilterApi(String task_type, String groupid) {

        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<GetTasksResponceModel> call = service.filter_task_group(task_type, groupid);
        call.enqueue(new Callback<GetTasksResponceModel>() {
            @Override
            public void onResponse(Call<GetTasksResponceModel> call, Response<GetTasksResponceModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                datalist = response.body().getTasksDataModels();
                if (status == 200) {
                    dialog0.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    setData(ParentRecyclerViewItem, datalist);
                } else {
                    dialog0.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetTasksResponceModel> call, Throwable t) {

                dialog0.dismiss();
                t.printStackTrace();
            }
        });

    }
}