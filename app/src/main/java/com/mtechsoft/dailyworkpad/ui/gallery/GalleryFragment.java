package com.mtechsoft.dailyworkpad.ui.gallery;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anggastudio.spinnerpickerdialog.SpinnerPickerDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.AddPlanResponceModel;
import com.mtechsoft.dailyworkpad.ViewModel.GetPlaningViewModel;
import com.mtechsoft.dailyworkpad.adapter.PlaningAdapter;
import com.mtechsoft.dailyworkpad.dataModel.GetPlaningDataModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryFragment extends Fragment implements PlaningAdapter.CallBack {
    private RecyclerView category_recyclerview;
    private PlaningAdapter adpter;
    private LinearLayoutManager manager;
    List<GetPlaningDataModel> models;
    public static final int REQUEST_IMAGE = 100;
    KProgressHUD progressHUD;
    int datePicker = 0;
    Context mcon;
    View v;
    KProgressHUD dialog0;
    //dialog box
    TextView ed_date, ed_enddate, ed_sartdate;
    String getStartTime = "", getEndTime = "";
    EditText ed_hyperLink, et_description;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    final Calendar myCalendar = Calendar.getInstance();
    ImageButton img_addProfessional;
    String minuts = "", hours = "",
            imguribase64, TAG = "GalleryFragment";

    //recycler view
    GetPlaningViewModel planingViewModel;
    private GalleryViewModel galleryViewModel;
    GetDateService service;
    public int itemPosition;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_gallery, container, false);

        init(v);
        //getCategories ();
        clicks(v);

        setViewModels(v);
        return v;
    }


    //recyclerview data
    private void setViewModels(View v) {
        // Planing Observer
        Observer<List<GetPlaningDataModel>> activePlaningsListObserver = new Observer<List<GetPlaningDataModel>>() {
            @Override
            public void onChanged(List<GetPlaningDataModel> dataModels) {
                if (dataModels.size() == 0) {
                    Toast.makeText(mcon, "No Orders found!", Toast.LENGTH_SHORT).show();
                    //layoutRecentlySaved.setVisibility(View.GONE);
                } else {
                    //category_recyclerview.setHasFixedSize(true);
                    adpter = new PlaningAdapter(getActivity(), dataModels, GalleryFragment.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(mcon, LinearLayoutManager.VERTICAL, false);
                    category_recyclerview.setLayoutManager(layoutManager);
                    category_recyclerview.setAdapter(adpter);
                }

            }
        };


        // Working With Active Orders
        category_recyclerview = v.findViewById(R.id.category_recyclerview);
        planingViewModel = new ViewModelProvider(this).get(GetPlaningViewModel.class);
        String userId = Utilities.getString(mcon, Utilities.USER_ID);
        //String userId="2";
        planingViewModel.getAllPlanings(userId, getContext());
        planingViewModel.getGetPlaning().observe(getViewLifecycleOwner(), activePlaningsListObserver);
    }

    private void clicks(View v) {
        img_addProfessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addnewItem();
            }
        });
    }

    private void init(View v) {
        models = new ArrayList<>();
        category_recyclerview = v.findViewById(R.id.category_recyclerview);
        img_addProfessional = v.findViewById(R.id.img_addProfessional);
        mcon = getActivity();
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
//        progressHUD = KProgressHUD.create(this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setWindowColor(getResources().getColor(R.color.colorBlack))
//                .setAnimationSpeed(3)
//                .setDimAmount(0.5f)
//                .setCancellable(true)
//                .setLabel("Please Wait")
//                .setCornerRadius(10)
//                .show();
    }

    private void addnewItem() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Dialog dialog;
        dialog = new Dialog(mcon);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addplan);
        dialog.getWindow().setLayout((int) (width * 0.9), (int) (height * 0.80));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ed_date = dialog.findViewById(R.id.ed_date);
        ed_hyperLink = dialog.findViewById(R.id.ed_hyperLink);
        ed_sartdate = dialog.findViewById(R.id.ed_sartdate);
        ed_enddate = dialog.findViewById(R.id.ed_enddate);
        et_description = dialog.findViewById(R.id.et_description);
        Button btn_addPlan = dialog.findViewById(R.id.btn_addPlan);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        ImageView img_certificate = dialog.findViewById(R.id.img_certificate);
        clicksOnDialog(imgclose, img_certificate, btn_addPlan, ed_date, dialog, ed_sartdate, ed_enddate);


        dialog.setCancelable(false);
        dialog.show();
    }


    //total clicks on Dialog add
    private void clicksOnDialog(ImageView imgclose, ImageView img_certificate, Button btn_addPlan, TextView ed_date, Dialog dialog, TextView ed_sartdate, TextView ed_enddate) {
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_addPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = ed_date.getText().toString().trim();
                String hyperLink = ed_hyperLink.getText().toString();
                String description = et_description.getText().toString();
                if (date.isEmpty()) {
                    ed_date.setError("*Date is Missing");
                    ed_date.requestFocus();
                } else if (getEndTime.isEmpty()) {
                    ed_enddate.setError("*End Time is Missing");
                    ed_enddate.requestFocus();
                } else if (getStartTime.isEmpty()) {
                    ed_sartdate.setError("*Start Time is Missing");
                    ed_sartdate.requestFocus();
                } else if (hyperLink.isEmpty()) {
                    ed_hyperLink.setError("*Hyper Link is Missing");
                    ed_hyperLink.requestFocus();
                } else if (description.isEmpty()) {
                    et_description.setError("*Description is Missing");
                    et_description.requestFocus();
                } else {
                    //progressLoading.show();
                    addPlan(description, hyperLink, getStartTime, getEndTime, date, dialog);
                }
            }
        });


        //date picker
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        ed_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SpinnerPickerDialog spinnerPickerDialog = new SpinnerPickerDialog();
                spinnerPickerDialog.setContext(getActivity());
                spinnerPickerDialog.setOnDialogListener(new SpinnerPickerDialog.OnDialogListener() {

                    @Override
                    public void onSetDate(int month, int day, int year) {
                        // "  (Month selected is 0 indexed {0 == January})"
                        String date = (month + 1) + "-" + day + "-" + year;
                        ed_date.setText(date);
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onDismiss() {

                    }

                });
                spinnerPickerDialog.show(getActivity().getSupportFragmentManager(), "");
            }
        });

        ed_sartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker = 2;
                final Calendar myCalender = Calendar.getInstance();
                int hour = myCalender.get(Calendar.HOUR_OF_DAY);
                int minute = myCalender.get(Calendar.MINUTE);


                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);
                            ed_sartdate.setText(hourOfDay + ":" + minute);
                            getStartTime = hourOfDay + ":" + minute;

                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
                timePickerDialog.setTitle("Choose hour:");
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });

        ed_enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker = 3;
                final Calendar myCalender = Calendar.getInstance();
                int hour = myCalender.get(Calendar.HOUR_OF_DAY);
                int minute = myCalender.get(Calendar.MINUTE);


                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);
                            ed_enddate.setText(hourOfDay + ":" + minute);
                            getEndTime = hourOfDay + ":" + minute;

                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
                timePickerDialog.setTitle("Choose hour:");
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });
    }

    private void addPlan(String description, String hyperLink, String startDate, String endDate, String date, Dialog dialog) {
        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        String user_id = Utilities.getString(mcon, Utilities.USER_ID);
        Call<AddPlanResponceModel> call = service.addPlanning(user_id, date, hyperLink, description, startDate, endDate);
        call.enqueue(new Callback<AddPlanResponceModel>() {
            @Override
            public void onResponse(Call<AddPlanResponceModel> call, Response<AddPlanResponceModel> response) {
                dialog0.dismiss();
                assert response.body() != null;
                int status = response.body().getStatus();
                Log.d(TAG, "status:\t" + status);
                String message = response.body().getMessage();
                if (status == 200) {
                    Utilities.makeToast(mcon, "Plan Added Successfully" + status);
                    Log.d(TAG, "Congradulations\t");
                    setViewModels(v);
                    dialog.dismiss();
                    // Working with response and saving data in Shared Preferences
                } else if (status == 400) {
                    dialog0.dismiss();
                    Utilities.makeToast(mcon, "Failed to Add Plan\n" + message);
                }

            }

            @Override
            public void onFailure(Call<AddPlanResponceModel> call, Throwable t) {
                dialog0.dismiss();
                Toast.makeText(mcon, t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        switch (datePicker) {
            case 1:
                ed_date.setText(sdf.format(myCalendar.getTime()));
                datePicker = 0;
                break;
            case 2:
                ed_sartdate.setText(sdf.format(myCalendar.getTime()));
                datePicker = 0;
                break;
            case 3:
                ed_enddate.setText(sdf.format(myCalendar.getTime()));
                datePicker = 0;
                break;
            default:
                datePicker = 0;
        }
    }

    @Override
    public void onGetPosition(int pos) {
        itemPosition = pos;
        Log.d(TAG, "Position" + itemPosition);
    }
}