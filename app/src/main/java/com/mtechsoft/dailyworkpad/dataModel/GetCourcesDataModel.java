package com.mtechsoft.dailyworkpad.dataModel;

import com.google.gson.annotations.SerializedName;

public class GetCourcesDataModel {
    @SerializedName("id")
    private int id;


    @SerializedName("user_id")
    private int user_id;

    @SerializedName("certificate_image")
    private String certificate_image;

    @SerializedName("course_date")
    private String course_date;


    @SerializedName("course_name")
    private String course_name;

    @SerializedName("provider_name")
    private String provider_name;

    @SerializedName("presenter_name")
    private String presenter_name;


    public GetCourcesDataModel() {
    }

    public GetCourcesDataModel(int id, int user_id, String certificate_image, String course_date, String course_name, String provider_name, String presenter_name) {
        this.id = id;
        this.user_id = user_id;
        this.certificate_image = certificate_image;
        this.course_date = course_date;
        this.course_name = course_name;
        this.provider_name = provider_name;
        this.presenter_name = presenter_name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCertificate_image() {
        return certificate_image;
    }

    public void setCertificate_image(String certificate_image) {
        this.certificate_image = certificate_image;
    }

    public String getCourse_date() {
        return course_date;
    }

    public void setCourse_date(String course_date) {
        this.course_date = course_date;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getPresenter_name() {
        return presenter_name;
    }

    public void setPresenter_name(String presenter_name) {
        this.presenter_name = presenter_name;
    }
}
