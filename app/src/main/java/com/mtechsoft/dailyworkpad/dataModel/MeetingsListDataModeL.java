package com.mtechsoft.dailyworkpad.dataModel;

import com.google.gson.annotations.SerializedName;

public class MeetingsListDataModeL {
    @SerializedName("id")
    private int id;

    @SerializedName("meeting_note_id")
    private int meeting_note_id;

    @SerializedName("meeting_title")
    private String meeting_title;

    public MeetingsListDataModeL() {
    }

    public MeetingsListDataModeL(int id, int meeting_note_id, String meeting_title) {
        this.id = id;
        this.meeting_note_id = meeting_note_id;
        this.meeting_title = meeting_title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMeeting_note_id() {
        return meeting_note_id;
    }

    public void setMeeting_note_id(int meeting_note_id) {
        this.meeting_note_id = meeting_note_id;
    }

    public String getMeeting_title() {
        return meeting_title;
    }

    public void setMeeting_title(String meeting_title) {
        this.meeting_title = meeting_title;
    }
}
