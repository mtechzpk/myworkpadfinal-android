package com.mtechsoft.dailyworkpad.dataModel;

import com.google.gson.annotations.SerializedName;

public class UserDataModel {

    @SerializedName("id")
    private int id;


    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    public UserDataModel() {
    }

    public UserDataModel(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
