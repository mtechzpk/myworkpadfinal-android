package com.mtechsoft.dailyworkpad.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTasksDataModel {
    @SerializedName("id")
    private int id;


    @SerializedName("user_id")
    private int user_id;

    @SerializedName("task_group_name")
    private String task_group_name;

    @SerializedName("task_group_date")
    private String task_group_date;


    @SerializedName("created_at")
    private String created_at;

    @SerializedName("task_list")
    private List<TaskListDataModel> taskListDataModels;


    public GetTasksDataModel() {
    }

    public GetTasksDataModel(int id, int user_id, String task_group_name, String task_group_date, String created_at, List<TaskListDataModel> taskListDataModels) {
        this.id = id;
        this.user_id = user_id;
        this.task_group_name = task_group_name;
        this.task_group_date = task_group_date;
        this.created_at = created_at;
        this.taskListDataModels = taskListDataModels;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getTask_group_name() {
        return task_group_name;
    }

    public void setTask_group_name(String task_group_name) {
        this.task_group_name = task_group_name;
    }

    public String getTask_group_date() {
        return task_group_date;
    }

    public void setTask_group_date(String task_group_date) {
        this.task_group_date = task_group_date;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<TaskListDataModel> getTaskListDataModels() {
        return taskListDataModels;
    }

    public void setTaskListDataModels(List<TaskListDataModel> taskListDataModels) {
        this.taskListDataModels = taskListDataModels;
    }
}
