package com.mtechsoft.dailyworkpad.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMeetingsDataModel {
    @SerializedName("id")
    private int id;

    @SerializedName("user_id")
    private int user_id;

    @SerializedName("meeting_note_title")
    private String meeting_note_title;

    @SerializedName("meeting_note_date")
    private String meeting_note_date;

    @SerializedName("meeting_list")
    private List<MeetingsListDataModeL> meeting_list;

    public GetMeetingsDataModel() {
    }

    public GetMeetingsDataModel(int id, String meeting_note_title, String meeting_note_date, List<MeetingsListDataModeL> meeting_list) {
        this.id = id;
        this.meeting_note_title = meeting_note_title;
        this.meeting_note_date = meeting_note_date;
        this.meeting_list = meeting_list;
    }

    public GetMeetingsDataModel(int id, int user_id, String meeting_note_title, String meeting_note_date, List<MeetingsListDataModeL> meeting_list) {
        this.id = id;
        this.user_id = user_id;
        this.meeting_note_title = meeting_note_title;
        this.meeting_note_date = meeting_note_date;
        this.meeting_list = meeting_list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getMeeting_note_title() {
        return meeting_note_title;
    }

    public void setMeeting_note_title(String meeting_note_title) {
        this.meeting_note_title = meeting_note_title;
    }

    public String getMeeting_note_date() {
        return meeting_note_date;
    }

    public void setMeeting_note_date(String meeting_note_date) {
        this.meeting_note_date = meeting_note_date;
    }

    public List<MeetingsListDataModeL> getMeeting_list() {
        return meeting_list;
    }

    public void setMeeting_list(List<MeetingsListDataModeL> meeting_list) {
        this.meeting_list = meeting_list;
    }
}
