package com.mtechsoft.dailyworkpad.dataModel;

import com.google.gson.annotations.SerializedName;

public class TaskListDataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("task_group_id")
    private int task_group_id;
    @SerializedName("task_name")
    private String task_name;
    @SerializedName("status")
    private String status;

    public TaskListDataModel() {
    }

    public TaskListDataModel(int id, int task_group_id, String task_name, String status) {
        this.id = id;
        this.task_group_id = task_group_id;
        this.task_name = task_name;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTask_group_id() {
        return task_group_id;
    }

    public void setTask_group_id(int task_group_id) {
        this.task_group_id = task_group_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
