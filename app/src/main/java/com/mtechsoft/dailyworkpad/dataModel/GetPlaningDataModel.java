package com.mtechsoft.dailyworkpad.dataModel;

import com.google.gson.annotations.SerializedName;

public class GetPlaningDataModel {
    @SerializedName("id")
    private int id;


    @SerializedName("user_id")
    private int user_id;

    @SerializedName("planning_date")
    private String planning_date;

    @SerializedName("planning_hyperlink")
    private String planning_hyperlink;


    @SerializedName("planning_description")
    private String planning_description;

    @SerializedName("start_time")
    private String start_time;

    @SerializedName("end_time")
    private String end_time;

    public GetPlaningDataModel() {
    }

    public GetPlaningDataModel(int id, int user_id, String planning_date, String planning_hyperlink, String planning_description, String start_time, String end_time) {
        this.id = id;
        this.user_id = user_id;
        this.planning_date = planning_date;
        this.planning_hyperlink = planning_hyperlink;
        this.planning_description = planning_description;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPlanning_date() {
        return planning_date;
    }

    public void setPlanning_date(String planning_date) {
        this.planning_date = planning_date;
    }

    public String getPlanning_hyperlink() {
        return planning_hyperlink;
    }

    public void setPlanning_hyperlink(String planning_hyperlink) {
        this.planning_hyperlink = planning_hyperlink;
    }

    public String getPlanning_description() {
        return planning_description;
    }

    public void setPlanning_description(String planning_description) {
        this.planning_description = planning_description;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
