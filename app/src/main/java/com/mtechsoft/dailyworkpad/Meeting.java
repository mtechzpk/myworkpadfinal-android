package com.mtechsoft.dailyworkpad;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.ResponceModel.AddPlanResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.CommonResponceModel;
import com.mtechsoft.dailyworkpad.ResponceModel.GetMeetingsResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.GetMeetingsDataModel;
import com.mtechsoft.dailyworkpad.dataModel.MeetingsListDataModeL;
import com.mtechsoft.dailyworkpad.nestedRecyclerviewAdapter.ParentItemAdapter;
import com.mtechsoft.dailyworkpad.nested_recyclerviewModel.ParentItem;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.ui.gallery.GalleryViewModel;
import com.mtechsoft.dailyworkpad.ui.home.HomeViewModel;
import com.mtechsoft.dailyworkpad.utilities.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Meeting extends Fragment implements ParentItemAdapter.CallBack{
    public static final int REQUEST_IMAGE = 100;
    KProgressHUD progressHUD;
    int datePicker = 0, itemPosition;
    Context mcon;
    String selection="";
    View v;
    KProgressHUD progressDialog;
    List<String> forsearch = new ArrayList<>();
    //dialog box
    List<GetMeetingsDataModel> datalist;
    KProgressHUD dialog0;
    TextView ed_date;
    EditText ed_tasktitle, ed_task1, ed_task2;
    RelativeLayout rl_addmore, rl_removetask;
    ScrollView scrollableView;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    final Calendar myCalendar = Calendar.getInstance();
    ImageButton img_addProfessional, img_filter;
    String minuts = "", hours = "", imguribase64, TAG = "Meeting";
    GetDateService service;
    private GalleryViewModel galleryViewModel;
    LinearLayout ll_taskdelete, ll_edittext;
    private HomeViewModel homeViewModel;
    List<ParentItem> itemList;
    String user_id="";
    List<GetMeetingsDataModel> parentmodels;
    List<MeetingsListDataModeL> childModels;
    ParentItemAdapter adapter;
    RecyclerView ParentRecyclerViewItem;
    static int counter = 0;
    List<EditText> myArray;
    StringBuilder builder;
    LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.meeting_fragment, container, false);
         user_id = Utilities.getString(getContext(), Utilities.USER_ID);

        init(v);
        //getCategories ();
        clicks(v);
        GetAllMeetings(user_id);
        return v;
    }

    private void GetAllMeetings(String user_id) {

        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<GetMeetingsResponceModel> call = service.get_meetings(user_id);
        call.enqueue(new Callback<GetMeetingsResponceModel>() {
            @Override
            public void onResponse(Call<GetMeetingsResponceModel> call, Response<GetMeetingsResponceModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                datalist = response.body().getGetMeetingsDataModel();
                if (status == 200) {
                    dialog0.dismiss();

                    for (int i = 0; i < datalist.size(); i++) {
                        String meetingTitle = datalist.get(i).getMeeting_note_title();
                        forsearch.add(meetingTitle);
                    }

                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    setData(ParentRecyclerViewItem, datalist);
                } else {
                    dialog0.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetMeetingsResponceModel> call, Throwable t) {

                dialog0.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void setData(RecyclerView rvAvailableDate, List datalist) {

        adapter = new ParentItemAdapter(datalist, getContext(),Meeting.this);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        ParentRecyclerViewItem.setHasFixedSize(true);
        ParentRecyclerViewItem.setLayoutManager(manager);
        ParentRecyclerViewItem.setAdapter(adapter);
    }


    private void clicks(View v) {
        img_addProfessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addnewItem();
//                Intent intent=new Intent (mcon, AddProfessionalPlanningActivity.class);
//                startActivity (intent);

            }
        });

        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                filterDialog();

            }
        });
    }

    private void init(View v) {
        //models=new ArrayList<> ();
        img_addProfessional = v.findViewById(R.id.img_addProfessional);
        img_filter = v.findViewById(R.id.img_filter);
        mcon = getActivity();
        myArray = new ArrayList();
        parentmodels = new ArrayList<>();
        childModels = new ArrayList<>();
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        ParentRecyclerViewItem = v.findViewById(R.id.parent_recyclerview);
        // Initialise the Linear layout manager
        layoutManager = new LinearLayoutManager(mcon);

    }

    private void addnewItem() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Dialog dialog;
        dialog = new Dialog(mcon);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_task);
        dialog.getWindow().setLayout((int) (width * 0.9), WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ed_date = dialog.findViewById(R.id.ed_date);
        RelativeLayout rl_addmore, rl_removetask;
        ed_tasktitle = dialog.findViewById(R.id.ed_tasktitle);
        ed_task2 = dialog.findViewById(R.id.ed_task2);
        ed_task1 = dialog.findViewById(R.id.ed_task1);
        rl_addmore = dialog.findViewById(R.id.rl_addmore);
        rl_removetask = dialog.findViewById(R.id.rl_removetask);
        Button btn_addTask = dialog.findViewById(R.id.btn_addTask);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        ll_taskdelete = dialog.findViewById(R.id.ll_taskdelete);
        TextView tv_giveyourcode = dialog.findViewById(R.id.tv_giveyourcode);
        ll_edittext = dialog.findViewById(R.id.ll_edittext);
        tv_giveyourcode.setText("Add Meeting Note");
        clicksOnDialog(imgclose, btn_addTask, ed_date, dialog, rl_addmore, rl_removetask, ll_edittext);


        dialog.setCancelable(false);
        dialog.show();
    }

    private void clicksOnDialog(ImageView imgclose, Button btn_addTask, TextView ed_date, Dialog dialog, RelativeLayout rl_addmore, RelativeLayout rl_removetask, LinearLayout ll_edittext) {
        rl_addmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "editText");
                addNewEditText();

            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        rl_removetask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_taskdelete.setVisibility(View.GONE);
            }
        });

        btn_addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = ed_date.getText().toString().trim();
                String taskTitle = ed_tasktitle.getText().toString();
                String task1 = ed_task1.getText().toString();
                Log.d(TAG, "Length:\t" + myArray.size());
                builder = new StringBuilder(task1);
                int i = 0;
                while (i < myArray.size()) {
                    Log.d(TAG, "Text for :\t" + myArray.get(i).getText().toString());
                    String finalString = myArray.get(i).getText().toString();
                    if (!finalString.isEmpty()) builder.append("," + finalString);
                    i++;
                }
                Log.d(TAG, "builder :\t" + builder);
                if (date.isEmpty()) {
                    ed_date.setError("*Missing");
                    ed_date.requestFocus();
                } else if (taskTitle.isEmpty()) {
                    ed_tasktitle.setError("*Missing");
                    ed_tasktitle.requestFocus();
                } else if (builder == null || builder.length() == 0) {
                    ed_task1.setError("*Missing");
                    ed_task1.requestFocus();
                } else {
                    AddMeetingApiCall(date, taskTitle, builder, dialog);
                }
            }
        });

        //date picker
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        ed_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(mcon, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void AddMeetingApiCall(String date, String meetingTitle, StringBuilder meetingsList, Dialog dialog) {
        String user_id = Utilities.getString(mcon, Utilities.USER_ID);
        Call<AddPlanResponceModel> call = service.addMeeting(user_id, meetingTitle, date, meetingsList.toString());
        call.enqueue(new Callback<AddPlanResponceModel>() {
            @Override
            public void onResponse(Call<AddPlanResponceModel> call, Response<AddPlanResponceModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                Log.d(TAG, "status:\t" + status);
                String message = response.body().getMessage();
                if (status == 200) {
                    Utilities.makeToast(mcon, "Meeting Added Successfully" + status);
                    GetAllMeetings(user_id);
                    dialog.dismiss();
                    // Working with response and saving data in Shared Preferences
                } else if (status == 400) {
                    //progressLoading.hide();
                    dialog.dismiss();
                    Utilities.makeToast(mcon, "Failed to Add Course\n" + message);
                }

            }

            @Override
            public void onFailure(Call<AddPlanResponceModel> call, Throwable t) {
                //progressLoading.hide();
                Toast.makeText(mcon, t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addNewEditText() {
        EditText editText = new EditText(mcon);
        String abc = editText.toString() + counter;
        editText.setId(counter);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 8, 20, 8);
        editText.setLayoutParams(params);
        editText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        editText.setBackground(getResources().getDrawable(R.drawable.bg_edittext_shape));
        editText.setHint("Add Task");
        editText.setTextSize(20);
        //editText.setTextCursorDrawable (getResources ().getDrawable (R.drawable.edit_cursor_color));
        editText.setPadding(10, 12, 10, 12);
        ll_edittext.addView(editText);
        myArray.add(editText);
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ed_date.setText(sdf.format(myCalendar.getTime()));
    }

    private List<GetMeetingsDataModel> ParentItemList(List<GetMeetingsDataModel> parentmodels) {
        int i = 0;
        Log.d(TAG, "Size:\t" + parentmodels.size());
        for (i = 0; i <= parentmodels.size() - 1; i++) {
            int id = parentmodels.get(i).getId();
            String meeting_note_title = parentmodels.get(i).getMeeting_note_title();
            String meeting_note_date = parentmodels.get(i).getMeeting_note_date();
            Log.d(TAG, "meeting_note_title:\t" + meeting_note_title);
            childModels = parentmodels.get(i).getMeeting_list();
        }
        return this.parentmodels;
    }

    private void filterDialog() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.meeting_filter);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.42)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnFilter = dialog.findViewById(R.id.btnFilter);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        AutoCompleteTextView autoCompleteTextView = dialog.findViewById(R.id.textview);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, forsearch);
        autoCompleteTextView.setAdapter(arrayAdapter);

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selection = (String) parent.getItemAtPosition(position);
                Toast.makeText(getContext(), ""+selection, Toast.LENGTH_SHORT).show();
            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(mcon, "get", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void deleteDialog(int id) {
        final PrettyDialog pDialog = new PrettyDialog(getActivity());
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete this Group?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_gray,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deletegrouppApi(id);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_gray,
                        R.color.colorWhite,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void deletegrouppApi(int idd) {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CommonResponceModel> call = service.delete_task_group(String.valueOf(idd));

        call.enqueue(new Callback<CommonResponceModel>() {
            @Override
            public void onResponse(Call<CommonResponceModel> call, Response<CommonResponceModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    GetAllMeetings(user_id);
                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponceModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }
    @Override
    public void onGetPosition(int pos) {
//        deleteDialog(datalist.get(pos).getId());
        Toast.makeText(mcon, "Under Developing", Toast.LENGTH_SHORT).show();
    }
}