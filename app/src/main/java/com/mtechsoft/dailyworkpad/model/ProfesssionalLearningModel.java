package com.mtechsoft.dailyworkpad.model;

public class ProfesssionalLearningModel {
    String tv_yearName,img;

    public ProfesssionalLearningModel() {
    }

    public ProfesssionalLearningModel(String tv_yearName, String img) {
        this.tv_yearName = tv_yearName;
        this.img = img;
    }

    public String getTv_yearName() {
        return tv_yearName;
    }

    public void setTv_yearName(String tv_yearName) {
        this.tv_yearName = tv_yearName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
