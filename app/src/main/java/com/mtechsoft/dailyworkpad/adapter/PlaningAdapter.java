package com.mtechsoft.dailyworkpad.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.CommonResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.GetPlaningDataModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaningAdapter extends RecyclerView.Adapter<PlaningAdapter.GetPlaningDataModelViewHolder> {
    Context context;
    List<GetPlaningDataModel> list;
    CallBack callBack;
    KProgressHUD progressDialog;

    public PlaningAdapter(Context context, List<GetPlaningDataModel> list, CallBack callBack) {
        this.context = context;
        this.list = list;
        this.callBack = callBack;
    }

    public PlaningAdapter(Context context, List<GetPlaningDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public GetPlaningDataModelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_planning, parent, false);
        return new GetPlaningDataModelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GetPlaningDataModelViewHolder holder, final int position) {
        GetPlaningDataModel model = list.get(position);
        //passing data in view elements
        TextView tv_yearName;
        ImageView img_moveForword;
        CardView itemID;


//        SimpleDateFormat sdf_ = new SimpleDateFormat("EEEE");
//        Log.d ("ABC","SimpleDateFormat:\t"+sdf_);
//        Date date = new Date();
//        //date.setDate (Integer.valueOf (model.getPlanning_date ()));
//        Log.d ("ABC","date:\t"+date);
//        String dayName = sdf_.format(date);

//        String dateStr = "14-08-2016";
//        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//        Date date = null;
//        try {
//            date = (Date)formatter.parse(dateStr);
//
//        } catch (ParseException e) {
//            e.printStackTrace ();
//        }
//        Log.d ("ABC","Date:\t"+date.getDay ()+1);
        //holder.tv_dayName.setText(date.getDay ());


//        long milliSeconds = Long.parseLong(model.getPlanning_date ());
//        Date resultDate = new Date(milliSeconds);
//        PrettyTime prettyTime = new PrettyTime();
//        holder.tv_dayName.setText(prettyTime.format(resultDate));
//        holder.tv_date.setText(model.getPlanning_date ());

        holder.tv_date.setText(model.getPlanning_date());
        //String deliveryDate=model.getPlanning_date ();
        SimpleDateFormat dateFormatprev = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = dateFormatprev.parse(model.getPlanning_date());
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd MMM yyyy");
            String changedDate = dateFormat.format(d.getDay());
            Log.d("TAA", "changedDate:\t" + changedDate);
            String[] arrayString = changedDate.split(" ");

            String date = arrayString[0];
            String title = arrayString[1];
            String body = arrayString[2];
//            holder.tv_dayName.setText(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.rl_planing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog(model.getId(), position);

            }
        });

        holder.img_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlanDataDialog(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class GetPlaningDataModelViewHolder extends RecyclerView.ViewHolder {
        TextView tv_dayName, tv_date;
        ImageView img_btn;
        RelativeLayout rl_planing;

        public GetPlaningDataModelViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_dayName = itemView.findViewById(R.id.tv_dayName);
            tv_date = itemView.findViewById(R.id.tv_date);
            img_btn = itemView.findViewById(R.id.img_btn);
            rl_planing = itemView.findViewById(R.id.rl_planing);
        }
    }

    public interface CallBack {
        void onGetPosition(int pos);
    }

    private void deleteDialog(int id, int postion) {
        final PrettyDialog pDialog = new PrettyDialog(context);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete this Planning?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_gray,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deletePlanningApi(id, postion);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_gray,
                        R.color.colorWhite,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void deletePlanningApi(int idd, int position) {
        progressDialog = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CommonResponceModel> call = service.deleteplanning(String.valueOf(idd));

        call.enqueue(new Callback<CommonResponceModel>() {
            @Override
            public void onResponse(Call<CommonResponceModel> call, Response<CommonResponceModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    list.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, list.size());
                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(context, response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponceModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void showPlanDataDialog(int postion) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addplan);
        dialog.getWindow().setLayout((int) (width * 0.90), (int) (height * 0.82)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView ed_date = dialog.findViewById(R.id.ed_date);
        TextView tv_giveyourcode = dialog.findViewById(R.id.tv_giveyourcode);
        EditText ed_hyperLink = dialog.findViewById(R.id.ed_hyperLink);
        TextView ed_sartdate = dialog.findViewById(R.id.ed_sartdate);
        TextView ed_enddate = dialog.findViewById(R.id.ed_enddate);
        EditText et_description = dialog.findViewById(R.id.et_description);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        ImageView img_certificate = dialog.findViewById(R.id.img_certificate);
        Button btn_addPlan = dialog.findViewById(R.id.btn_addPlan);
        btn_addPlan.setVisibility(View.GONE);
        et_description.setEnabled(false);
        ed_hyperLink.setEnabled(false);
        tv_giveyourcode.setText(" Lesson Plan");
        ed_date.setText(list.get(postion).getPlanning_date());
        ed_sartdate.setText(list.get(postion).getStart_time());
        ed_hyperLink.setText(list.get(postion).getPlanning_hyperlink());
        et_description.setText(list.get(postion).getPlanning_description());
        ed_enddate.setText(list.get(postion).getEnd_time());
        et_description.setText(list.get(postion).getPlanning_description());
//        Picasso.get().load(list.get(postion).)
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                list.get(postion).
            }
        });


        dialog.show();
    }
}




