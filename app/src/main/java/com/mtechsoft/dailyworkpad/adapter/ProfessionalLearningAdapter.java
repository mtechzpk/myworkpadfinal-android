package com.mtechsoft.dailyworkpad.adapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.CommonResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.GetCourcesDataModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;
import com.mtechsoft.dailyworkpad.utilities.CommonMethodes;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfessionalLearningAdapter extends RecyclerView.Adapter<ProfessionalLearningAdapter.ProfesssionalLearningModelViewHolder> {
    Context context;
    List<GetCourcesDataModel> list;
    CallBack callBack;
    KProgressHUD progressDialog;

    public ProfessionalLearningAdapter(Context context, List<GetCourcesDataModel> list, CallBack callBack) {
        this.context = context;
        this.list = list;
        this.callBack = callBack;
    }

    final Calendar myCalendar = Calendar.getInstance();

    public ProfessionalLearningAdapter(Context context, List<GetCourcesDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ProfesssionalLearningModelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_professionallearning, parent, false);
        return new ProfesssionalLearningModelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfesssionalLearningModelViewHolder holder, final int position) {
        GetCourcesDataModel model = list.get(position);
        //passing data in view elements
        TextView tv_yearName;
        ImageView img_moveForword;
        CardView itemID;
        holder.tv_yearName.setText(CommonMethodes.splitDate(model.getCourse_date()));
        holder.itemID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteApi(list.get(position).getId(), position);
            }
        });
 holder.img_moveForword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showProfessionalLearningData(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ProfesssionalLearningModelViewHolder extends RecyclerView.ViewHolder {
        TextView tv_yearName;
        ImageView img_moveForword;
        CardView itemID;

        public ProfesssionalLearningModelViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_yearName = itemView.findViewById(R.id.tv_yearName);
            img_moveForword = itemView.findViewById(R.id.img_moveForword);
            itemID = itemView.findViewById(R.id.itemID);
        }
    }

    public interface CallBack {
        void onGetPosition(int pos);
    }


    private void deleteApi(int id, int postion) {
        final PrettyDialog pDialog = new PrettyDialog(context);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete this ? ")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_gray,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deleteItemApi(id, postion);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_gray,
                        R.color.colorWhite,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void deleteItemApi(int idd, int position) {
        progressDialog = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CommonResponceModel> call = service.deletelearningcourse(String.valueOf(idd));

        call.enqueue(new Callback<CommonResponceModel>() {
            @Override
            public void onResponse(Call<CommonResponceModel> call, Response<CommonResponceModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    list.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, list.size());

                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(context, response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponceModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void showProfessionalLearningData(int position) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Dialog dialog;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_add_professional_planning);
        dialog.getWindow().setLayout((int) (width * 0.9), (int) (height * 0.90));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView ed_date = dialog.findViewById(R.id.ed_date);
        EditText ed_courseName = dialog.findViewById(R.id.ed_courseName);
        EditText ed_PresenterName = dialog.findViewById(R.id.ed_PresenterName);
        EditText ed_ProviderName = dialog.findViewById(R.id.ed_ProviderName);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        Button btn_continue = dialog.findViewById(R.id.btn_continue);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        ImageView img_certificate = dialog.findViewById(R.id.img_certificate);
        tvTitle.setText("Learning Course");
        ed_PresenterName.setEnabled(false);
        ed_courseName.setEnabled(false);
        ed_ProviderName.setEnabled(false);
        btn_continue.setVisibility(View.GONE);
        ed_date.setText(list.get(position).getCourse_date());
        ed_courseName.setText(list.get(position).getCourse_name());
        ed_PresenterName.setText(list.get(position).getPresenter_name());
        ed_ProviderName.setText(list.get(position).getProvider_name());
        Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG+list.get(position).getCertificate_image()).placeholder(R.drawable.add_img).into(img_certificate);



        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }
}



