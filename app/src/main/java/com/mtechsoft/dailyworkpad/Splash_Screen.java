package com.mtechsoft.dailyworkpad;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.mtechsoft.dailyworkpad.activities.LogInActivity;
import com.mtechsoft.dailyworkpad.utilities.Utilities;

public class Splash_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        String loginStatus = Utilities.getString(Splash_Screen.this, Utilities.Login_Status);
        if (loginStatus.equals("logged")) {

            Thread background = new Thread() {
                public void run() {
                    try {
                        // Thread will sleep for 5 seconds
                        sleep(3 * 1000);

                        // After 5 seconds redirect to another intent
                        Intent i = new Intent(getBaseContext(),Home_Screen.class);
                        startActivity(i);

                        //Remove activity
                        finish();
                    } catch (Exception e) {
                    }
                }
            };
            // start thread
            background.start();
        } else {
            Thread background = new Thread() {
                public void run() {
                    try {
                        // Thread will sleep for 5 seconds
                        sleep(3 * 1000);

                        // After 5 seconds redirect to another intent
                        Intent i = new Intent(getBaseContext(), LogInActivity.class);
                        startActivity(i);

                        //Remove activity
                        finish();
                    } catch (Exception e) {
                    }
                }
            };
            // start thread
            background.start();
        }


    }
}