package com.mtechsoft.dailyworkpad.utilities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Utilities {


    //    public static final String IMAGE_URL = "http://mtecsoft.com/servlly/public/";
    public static final String IMAGE_URL = "http://etisalatconnection.com/servlly/public/";

    public static final String MAP_KEY = "AIzaSyDqCH1HTTLdcchBvgZQf2nAw-lFZ4sXkjE";
    public static final String DB_NAME = "DailyWorkLoad";
    public static final String USER_ID = "user_id";
    public static final String USER_TYPE = "user_type";
    public static final String BUYER = "buyer";
    public static final String PROVIDER = "provider";
    public static final String GUEST = "guest";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PROFILE_IMAGE = "user_profile_image";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_ADDRESS = "user_address";
    public static final String USER_LAT = "user_lat";
    public static final String USER_LON = "user_lon";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_BALANCE = "user_balance";
    public static final String USER_PROOF_ID = "user_proof_id";
    public static final String USER_DOB = "user_dob";
    public static final String USER_EXPERIENCE = "user_experience";
    public static final String USER_LOCATION = "user_location";

    public static final String IS_USER_ONLINE = "is_user_online";
    public static final String Login_Status = "logged_in";

    public static final String PROVIDER_ID = "provider_id";
    public static final String PROVIDER_NAME = "provider_name";
    public static final String PROVIDER_LAT = "provider_lat";
    public static final String PROVIDER_LNG = "provider_lng";
    public static final String PROVIDER_PROFILE_IMAGE = "provider_profile_image";
    public static final String PROVIDER_FEE = "provider_fee  ";
    public static final String PROVIDER_RATING = "provider_rating  ";
    public static final String PROVIDER_REVIEWS = "provider_reviews  ";

    public static final String BOOKING_ID = "booking_id";
    public static final String BOOKING_NAME = "booking_name";
    public static final String BOOKING_SERVICE = "booking_service";
    public static final String BOOKING_IMAGE = "booking_image";
    public static final String BOOKING_FEE = "booking_fee";
    public static final String BOOKING_PAYMENT_METHOD = "booking_payment_method";
    public static final String BOOKING_DATE = "booking_date";
    public static final String BOOKING_TIME = "booking_time";
    public static final String BOOKING_DATE_TIME = "booking_date_time";
    public static final String BOOKING_LAT = "booking_lat";
    public static final String BOOKING_LON = "booking_lon";
    public static final String BOOKING_OTP = "booking_lon";

    public static final String RECEIVER_ID = "receiver_id";
    public static final String RECEIVER_NAME = "receiver_name";
    public static final String RECEIVER_IMAGE = "receiver_image";
    public static final String RECEIVER_TOKEN = "receiver_token";


    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_TITLE = "category_title";
    public static final String SUBCATEGORY_ID = "subcategory_id";
    public static final String SERVICE_NAME = "service_name";
    public static final String SERVICE_FFE = "service_fee";
    public static final String SERVICE_IMAGE = "service_image";
    public static final String SERVICE_DESCRIPTION = "service_description";


    public static final String FORGOT_EMAIL = "forgot_email";
    public static final String ONLINE = "online";
    public static final String IS_SAVED = "is_saved";
    public static final String SELECTED_SERVICES_ID = "selected_services_id";
    public static final String SELECTED_SERVICES = "selected_services";
    public static final String SELECTED_SERVICES_IMAGES = "selected_services_images";

    public static final String SELECTED_LAT = "selected_lan";
    public static final String SELECTED_LON = "selected_lon";


    public static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit," +
            " sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim" +
            " veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo " +
            "consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum " +
            "dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, " +
            "sunt in culpa qui officia deserunt mollit anim id est laborum.";


    static ProgressDialog dialog;
    private Context context;


    public Utilities(Context context) {
        this.context = context;
    }


    public static void showProgressDialog(Context ctx, String msg) {
        dialog = new ProgressDialog(ctx);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(msg);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
            dialog = null;
        }

    }

    public static void makeToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void saveInt(Context context, String key, int value) {
        SharedPreferences sharedPref = context.getSharedPreferences(Utilities.DB_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getInt(Context context, String key) {

        SharedPreferences sharedPref = context.getSharedPreferences(Utilities.DB_NAME, Context.MODE_PRIVATE);
        int val = sharedPref.getInt(key, 0);
        return val;

    }

    public static void saveString(Context context, String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(Utilities.DB_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public static String getString(Context context, String key) {

        SharedPreferences sharedPref = context.getSharedPreferences(Utilities.DB_NAME, Context.MODE_PRIVATE);
        String val = sharedPref.getString(key, "");

        return val;

    }

    public static void clearSharedPref(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Utilities.DB_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();


    }

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    public static String changeDateFormate(String date, String from, String to) {

        String finalDate = date;

//        SimpleDateFormat date_formate = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat date_formate = new SimpleDateFormat(from);

        Date datee = null;
        try {
            datee = date_formate.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

//        SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
        SimpleDateFormat df = new SimpleDateFormat(to);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datee);
        finalDate = df.format(calendar.getTime());

        return finalDate;

    }

    public static String differenceBetweenDated(String dates_formate, String dateEnd, String dateStr) {

        String days = "";

        SimpleDateFormat sdf = new SimpleDateFormat(dates_formate);

        try {
            Date dateS = sdf.parse(dateStr);
            Date dateE = sdf.parse(dateEnd);

            long diff = dateE.getTime() - dateS.getTime();


            days = String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return days;

    }

    public static String getCurrentDate() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = mdformat.format(calendar.getTime());

        return currentDate;

    }

    public static String formattTwoDecimal(Context context, int number) {


        if (number == 0) {

            return String.valueOf("RS 0.00");
        } else {

            String qa = String.valueOf(number);
            String nocomma = qa.replace(",", "");
            Float f = Float.parseFloat(nocomma);
            String.format("%.2f", f);
            DecimalFormat formatter = new DecimalFormat("#,###,###.00#");
            String yourFormattedString = formatter.format(f);
            return String.valueOf("RS " + yourFormattedString);
        }

    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isConnected(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            return (mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting());
        } else
            return false;
    }

    public static AlertDialog.Builder buildNoInternetDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        return builder;
    }

//    public static BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
//        Canvas canvas = new Canvas();
//        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//        canvas.setBitmap(bitmap);
//        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
//        drawable.draw(canvas);
//        return BitmapDescriptorFactory.fromBitmap(bitmap);
//    }
//
//    public static String getAddress(Context context, LatLng latLng) {
//
//        if (latLng == null) {
//            return "";
//        }
//        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
//        List<Address> addresses = new ArrayList<>();
//
//        try {
//            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 3);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        if (!addresses.isEmpty())
//            return addresses.get(0).getAddressLine(0);
//        else
//            return "";
//    }


}
