package com.mtechsoft.dailyworkpad.nestedRecyclerviewAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.dataModel.GetTasksDataModel;

import java.util.List;


public class ParentItemAdapterForTasks
        extends RecyclerView.Adapter<ParentItemAdapterForTasks.ParentViewHolder> {

    // An object of RecyclerView.RecycledViewPool
    // is created to share the Views
    // between the child and
    // the parent RecyclerViews
    private RecyclerView.RecycledViewPool
            viewPool
            = new RecyclerView
            .RecycledViewPool();
    private List<GetTasksDataModel> itemList;
    CallBack callBack;
    Context context;
    KProgressHUD progressDialog;

    public ParentItemAdapterForTasks(List<GetTasksDataModel> itemList, Context context, CallBack callBack) {
        this.itemList = itemList;
        this.context = context;
        this.callBack = callBack;
    }


    @NonNull
    @Override
    public ParentItemAdapterForTasks.ParentViewHolder onCreateViewHolder(
            @NonNull ViewGroup viewGroup,
            int i) {

        // Here we inflate the corresponding
        // layout of the parent item
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(
                        R.layout.parent_item,
                        viewGroup, false);

        return new ParentItemAdapterForTasks.ParentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull ParentItemAdapterForTasks.ParentViewHolder parentViewHolder,
            int position) {
        parentViewHolder.bind(position);
        // Create an instance of the ParentItem
        // class for the given position
        GetTasksDataModel parentItem
                = itemList.get(position);

        parentViewHolder
                .ParentItemTitle
                .setText(parentItem.getTask_group_name());


//            parentViewHolder.ll_item.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//                    deleteDialog(parentItem.getId(),position);
//                    return true;
//                }
//            });

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(
                parentViewHolder
                        .ChildRecyclerView
                        .getContext(),
                LinearLayoutManager.VERTICAL,
                false);

        layoutManager
                .setInitialPrefetchItemCount(
                        parentItem
                                .getTaskListDataModels()
                                .size());
        ChildItemAdapterForTasks childItemAdapter
                = new ChildItemAdapterForTasks(parentItem.getTaskListDataModels(), context);
        parentViewHolder
                .ChildRecyclerView
                .setLayoutManager(layoutManager);
        parentViewHolder
                .ChildRecyclerView
                .setAdapter(childItemAdapter);
        parentViewHolder
                .ChildRecyclerView
                .setRecycledViewPool(viewPool);


    }

    @Override
    public int getItemCount() {

        return itemList.size();
    }

    // This class is to initialize
    // the Views present in
    // the parent RecyclerView
    class ParentViewHolder
            extends RecyclerView.ViewHolder {

        private TextView ParentItemTitle;
        private RecyclerView ChildRecyclerView;
        ImageView ivDelete;
        LinearLayout ll_item;

        ParentViewHolder(final View itemView) {
            super(itemView);

            ParentItemTitle = itemView.findViewById(R.id.parent_item_title);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            ChildRecyclerView
                    = itemView
                    .findViewById(
                            R.id.child_recyclerview);
            ll_item = itemView.findViewById(R.id.ll_item);
        }

        private void bind(int pos) {

            initClickListener();
        }

        private void initClickListener() {
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.onGetPosition(getAdapterPosition());
                }
            });
        }
    }

    public interface CallBack {
        void onGetPosition(int pos);
    }

}

