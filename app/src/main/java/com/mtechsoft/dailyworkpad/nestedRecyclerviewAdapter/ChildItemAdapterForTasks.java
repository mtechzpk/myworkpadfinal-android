package com.mtechsoft.dailyworkpad.nestedRecyclerviewAdapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.CommonResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.TaskListDataModel;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;

import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChildItemAdapterForTasks
        extends RecyclerView
        .Adapter<ChildItemAdapterForTasks.ChildViewHolder> {
    KProgressHUD progressDialog;
    private List<TaskListDataModel> ChildItemList;
    CallBack callBack;
    Context context;
    CheckBox chk_task;
    // Constuctor


    public ChildItemAdapterForTasks(List<TaskListDataModel> childItemList, Context context) {
        ChildItemList = childItemList;
        this.context = context;
    }

    public ChildItemAdapterForTasks(List<TaskListDataModel> childItemList, CallBack callBack, Context context) {
        ChildItemList = childItemList;
        this.callBack = callBack;
        this.context = context;
    }

    @NonNull
    @Override
    public ChildItemAdapterForTasks.ChildViewHolder onCreateViewHolder(
            @NonNull ViewGroup viewGroup,
            int i) {

        // Here we inflate the corresponding
        // layout of the child item
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(
                        R.layout.child_item,
                        viewGroup, false);

        return new ChildItemAdapterForTasks.ChildViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull ChildItemAdapterForTasks.ChildViewHolder childViewHolder,
            int position) {

        // Create an instance of the ChildItem
        // class for the given position
        TaskListDataModel childItem
                = ChildItemList.get(position);

        // For the created instance, set title.
        // No need to set the image for
        // the ImageViews because we have
        // provided the source for the images
        // in the layout file itself
        chk_task
                .setText(childItem.getTask_name());
        String statuss = childItem.getStatus();
        if (statuss.equals("pending")) {
            chk_task.setChecked(false);

        } else if (statuss.equals("completed")) {

            chk_task.setChecked(true);

        }
        chk_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chk_task.setChecked(true);

                showdialogue(childItem.getId(), position, statuss);
            }
        });

    }

    @Override
    public int getItemCount() {

        // This method returns the number
        // of items we have added
        // in the ChildItemList
        // i.e. the number of instances
        // of the ChildItemList
        // that have been created
        return ChildItemList.size();
    }

    // This class is to initialize
    // the Views present
    // in the child RecyclerView
    class ChildViewHolder
            extends RecyclerView.ViewHolder {

        LinearLayout ll_item;

        ChildViewHolder(View itemView) {
            super(itemView);
            chk_task = itemView.findViewById(R.id.chk_task);
            ll_item = itemView.findViewById(R.id.ll_item);

        }
    }

    public interface CallBack {
        void onGetPosition(int pos);
    }


    private void completeTask(int id, int position) {
        final PrettyDialog pDialog = new PrettyDialog(context);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Complete this Task ? ")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_gray,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {

                                pDialog.dismiss();
                                completeTaskApi(id, position);
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_gray,
                        R.color.colorWhite,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void showdialogue(int id, int postion, String status) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.task_options_dialog);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.42)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvComplete = dialog.findViewById(R.id.tvComplete);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        TextView tvDelete = dialog.findViewById(R.id.tvDelete);
        if (status.equals("completed")) {
            tvComplete.setVisibility(View.GONE);

        }
        tvComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completeTask(id, postion);
                dialog.dismiss();
            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteApi(id, postion);
            }
        });
        dialog.show();
    }

    private void deleteApi(int id, int postion) {
        final PrettyDialog pDialog = new PrettyDialog(context);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete this Task ? ")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_gray,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deleteItemApi(id, postion);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_gray,
                        R.color.colorWhite,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void deleteItemApi(int idd, int position) {
        progressDialog = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CommonResponceModel> call = service.deletesingleTask(String.valueOf(idd));

        call.enqueue(new Callback<CommonResponceModel>() {
            @Override
            public void onResponse(Call<CommonResponceModel> call, Response<CommonResponceModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    ChildItemList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, ChildItemList.size());

                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponceModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void completeTaskApi(int idd, int position) {
        progressDialog = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CommonResponceModel> call = service.complete_task(String.valueOf(idd));

        call.enqueue(new Callback<CommonResponceModel>() {
            @Override
            public void onResponse(Call<CommonResponceModel> call, Response<CommonResponceModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    if (message.equals("Task Completed!")) {
                        chk_task.setChecked(true);

                    }
                    ChildItemList.get(position).setStatus("completed");
                    notifyDataSetChanged();

                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponceModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

}
