package com.mtechsoft.dailyworkpad.nestedRecyclerviewAdapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtechsoft.dailyworkpad.R;
import com.mtechsoft.dailyworkpad.ResponceModel.CommonResponceModel;
import com.mtechsoft.dailyworkpad.dataModel.MeetingsListDataModeL;
import com.mtechsoft.dailyworkpad.networks.GetDateService;
import com.mtechsoft.dailyworkpad.networks.RetrofitClientInstance;

import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChildItemAdapter
        extends RecyclerView
        .Adapter<ChildItemAdapter.ChildViewHolder> {

    private List<MeetingsListDataModeL> ChildItemList;
    Context context;
    KProgressHUD progressDialog;

    // Constuctor
    ChildItemAdapter(List<MeetingsListDataModeL> childItemList, Context context) {
        this.ChildItemList = childItemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(
            @NonNull ViewGroup viewGroup,
            int i) {

        // Here we inflate the corresponding
        // layout of the child item
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(
                        R.layout.child_item_meeting,
                        viewGroup, false);

        return new ChildViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull ChildViewHolder childViewHolder,
            int position) {

        // Create an instance of the ChildItem
        // class for the given position
        MeetingsListDataModeL childItem
                = ChildItemList.get(position);

        // For the created instance, set title.
        // No need to set the image for
        // the ImageViews because we have
        // provided the source for the images
        // in the layout file itself
        childViewHolder
                .chk_task
                .setText(childItem.getMeeting_title());
//        childViewHolder.chk_task.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                return false;
//            }
//        });
        childViewHolder.chk_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteApi(ChildItemList.get(position).getId(), position);

            }
        });
    }

    @Override
    public int getItemCount() {

        // This method returns the number
        // of items we have added
        // in the ChildItemList
        // i.e. the number of instances
        // of the ChildItemList
        // that have been created
        return ChildItemList.size();
    }

    // This class is to initialize
    // the Views present
    // in the child RecyclerView
    class ChildViewHolder
            extends RecyclerView.ViewHolder {

        TextView chk_task;

        ChildViewHolder(View itemView) {
            super(itemView);
            chk_task
                    = itemView.findViewById(
                    R.id.chk_task);
        }
    }

    private void deleteApi(int id, int postion) {
        final PrettyDialog pDialog = new PrettyDialog(context);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete this ? ")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_gray,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deleteItemApi(id, postion);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_gray,
                        R.color.colorWhite,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void deleteItemApi(int idd, int position) {
        progressDialog = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CommonResponceModel> call = service.deleteMeeting(String.valueOf(idd));

        call.enqueue(new Callback<CommonResponceModel>() {
            @Override
            public void onResponse(Call<CommonResponceModel> call, Response<CommonResponceModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    ChildItemList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, ChildItemList.size());

                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(context, response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponceModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }
}